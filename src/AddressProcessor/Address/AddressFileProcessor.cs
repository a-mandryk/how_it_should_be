﻿using System;
using System.Collections.Generic;
using System.Linq;

using AddressProcessing.Address.v1;
using AddressProcessing.CSV;

namespace AddressProcessing.Address
{
    public class AddressFileProcessor
    {
        private readonly IMailShot _mailShot;
        private readonly Action<string, Exception> _logException;

        public AddressFileProcessor(IMailShot mailShot)
            : this(mailShot, (message, exception) => { })
        {
        }

        public AddressFileProcessor(IMailShot mailShot, Action<string, Exception> logException)
        {
            if (mailShot == null)
            {
                throw new ArgumentNullException("mailShot");
            }

            _mailShot = mailShot;
            _logException = logException;
        }

        public void Process(string inputFile)
        {
            try
            {
                var reader = new CSVReaderWriter();

                reader.Open(inputFile, CSVReaderWriter.Mode.Read);

                IEnumerable<string> columns;

                while (reader.Read(new []{0, 1}, out columns))
                {
                    _mailShot.SendMailShot(columns.ElementAt(0), columns.ElementAt(1));
                }

                reader.Close();
            }
            catch (Exception exception)
            {
                _logException("AddressFileProcessor failed", exception);

                throw;
            }
        }
    }
}