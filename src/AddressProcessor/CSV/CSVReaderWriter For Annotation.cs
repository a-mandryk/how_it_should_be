﻿// General suggestions:
//      - looks like solution was created with intention to use DI but it's implementation looks partial.
//      - solution structure - its better to use separated src & test & build folders (like many git based projects) so we can separate that functionalities. it's better to do that initilally instead of when solution will grow up and we will require it anyway
//      - "Assume this code is in production" - so our code should contain some logging strategy for issues troubleshooting.
//      - for better performance in solution we need to use async functionality. For example in _mailShot.SendMailShot(column1, column2); (AddressFileProcessor class) we will not wait untill message will be sent to process next line.

using System;
using System.IO;

namespace AddressProcessing.CSV
{
    //Names that have abbreviations in Uppercase are not so readable as witout all-in-uppercase. But it depends on common naming agreements.
    public class CSVReaderWriterForAnnotation
    {
        //fields initialization by default value unnecessary
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        //Mode enum could be extracted to separate file for better code consistency.
        //The flags attribute should be used when we need a collection of flags, rather than a single value.
        //In our implementation we don't use Read&Write so Flags is useless and if we are planning to add Read&Write only we can use Sustem.IO.FileAccess enum insted of custom enum.
        [Flags]
        public enum Mode { Read = 1, Write = 2 };

        public void Open(string fileName, Mode mode)
        {
            //Check for file exists required.
            
            //it's better to use switch/case for enum check.
            if (mode == Mode.Read)
            {
                //Add check to make sure we have not opened stream before
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                //Add check to make sure we have not opened stream before
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                //It's good to add Mode value to error message too. And as file mode is enum better to write "unsupported file mode"
                //Using corresponding exceptions (NotSupportedExcepton in our case) will help to keep errors more structured
                throw new Exception("Unknown file mode for " + fileName);
            }
        }

        //Method is not used and if doesn't used in external projects could be deleted
        public void Write(params string[] columns)
        {
            //"A junior developer" should know about StringBuilder first of all. 
            //Could be changed to string.Join() method

            //Use string.Empty. string.Empty is a read-only field whereas "" is a compile time constant and using string.Empty makes code more readable
            string outPut = "";

            //Check for columns == null requires
            for (int i = 0; i < columns.Length; i++)
            {
                outPut += columns[i];
                //every loop step we calculate columns.Length - 1. Extract outside for one time calculation or refactor.
                if ((columns.Length - 1) != i)
                {
                    //tab separator used in Write and Read methods need to be extracted to field (readonly);
                    outPut += "\t";
                }
            }

            WriteLine(outPut);
        }

        //What is purpose of this method and why we pass column1, column2 if we overwrite them inside method anyway. If we need this method it should be refactored, if no - removed as it has not any practical value.
        //For backward compatibility we will left method but refactor internal code
        public bool Read(string column1, string column2)
        {
            //it's better to extract to class fields so if we need change - it will be easy to locate.
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            //delcaltarion & assigment of line & columns must be joined.
            string line;
            string[] columns;

            //tab separator used in Write and Read methods and initializes on every Read run - need to be extracted to field (readonly);
            char[] separator = { '\t' };

            //ReadLine method is partially useful helper method and could to be refactored to ReadColums method.
            line = ReadLine();
            columns = line.Split(separator);

            //useless code - return columns.Length == 0 will be enough (if we assume that method is useful itself)
            //Possible issue when columns[FIRST_COLUMN] and column2 = columns[SECOND_COLUMN] are null values in array but we return true. Code will be refactore due to commet above also.
            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            //There is no columns.Length > SECOND_COLUMN check to avoid error. And better to check (columns.Length > FIRST_COLUMN) && (columns.Length > SECOND_COLUMN).
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        //Previous method and this one are pretty same - DRY
        //it's better to use for example IEnumerable<string> than add additional out string columns parameters to provide more columns in result.
        //This is generic implementation of SCV reader writer so column's indexes could be changed too - need to be included in method call
        //Possible it could be refactotred to something like Read<T>(out T result)
        public bool Read(out string column1, out string column2)
        {
            //Colum's indexes could be changed too - need to be included in method call
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            //declaration & assignment of line & columns must be joined.
            string line;
            string[] columns;

            //tab separator used in Write and Read methods and initializes on every Read run - need to be extracted to field (readonly);
            char[] separator = { '\t' };

            //ReadLine method is partially useful helper method and needs to be refactored to ReadColums method so further code will be changed too.
            line = ReadLine();

            if (line == null)
            {
                column1 = null;
                column2 = null;

                return false;
            }

            //Use line.Split(new[] {Delimiter}, 3, StringSplitOptions.None) as we need 1st and 2nd elements only and no need to split full line
            columns = line.Split(separator);

            //this condition and previous should be combined and extracted to seprate method IsReadFinished for example
            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                //false used in while(reader.Read(out column1, out column2)) so no need in columns null initialization before.
                return false;
            }
            //There is no columns.Length > SECOND_COLUMN check to avoid error. And better to check (columns.Length > FIRST_COLUMN) && (columns.Length > SECOND_COLUMN).
            else
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        //No need to write helper with small single line inside for single usage (IMHO)
        //We try to write but don't know mode of file was opened (ie possible _writerStream is null) - null check required
        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
        }

        //No need to write helper with small single line inside for single usage (IMHO)
        //We try to read but don't know mode of file was opened (ie possible _readStream is null) - null check required
        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        //Public methods (especially those who used in other classes) better to declare above private helpers methods
        public void Close()
        {
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
    }
}