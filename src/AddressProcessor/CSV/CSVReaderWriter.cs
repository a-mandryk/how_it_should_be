﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace AddressProcessing.CSV
{
    /// <summary>
    /// Class to read & write CSV files
    /// </summary>
    public class CSVReaderWriter
    {
        [Flags]
        public enum Mode
        {
            Read = 1,
            Write = 2
        };

        private string _delimiter;
        private StreamReader _readerStream;
        private StreamWriter _writerStream;

        /// <summary>
        /// Open CSV file in Read or Write mode
        /// </summary>
        /// <param name="fileName">Path to CSV file</param>
        /// <param name="mode">File open mode: Read/Write</param>
        /// <param name="delimiter">Columns delimiter. Default value: \t (TAB) </param>
        public void Open(string fileName, Mode mode, string delimiter = "\t")
        {
            CheckIsFileExistsOrCloseAndThrowException(fileName);

            switch (mode)
            {
                case Mode.Read:
                    OpenReader(fileName);
                    break;

                case Mode.Write:
                    OpenWriter(fileName);
                    break;

                default:
                    CloseAndThrowExcepton(new NotSupportedException(string.Format(CultureInfo.InvariantCulture, "Unsupported file mode {0} for file {1}", Enum.GetName(typeof(Mode), mode), fileName)));
                    break;
            }

            _delimiter = delimiter;
        }

        /// <summary>
        /// Write columns to file
        /// </summary>
        /// <param name="columns">Columns to write</param>
        public void Write(params string[] columns)
        {
            var output = IsNotNull(columns)
                ? string.Join(_delimiter, columns)
                : string.Empty;

            WriteLine(output);
        }

        /// <summary>
        /// Read columns from CSV file at specified indexes
        /// </summary>
        /// <param name="columnsIndexes">Columns indexes to extract</param>
        /// <param name="columns">Extracted colums</param>
        /// <returns>Continue read file</returns>
        public bool Read(IEnumerable<int> columnsIndexes, out IEnumerable<string> columns)
        {
            if (!IsNotNull(columnsIndexes))
            {
                CloseAndThrowExcepton(new ArgumentNullException("columnsIndexes"));
            }

            if (columnsIndexes.Any(index => index < 0))
            {
                CloseAndThrowExcepton(new ArgumentOutOfRangeException("columnsIndexes", "All column indexes should be greater or equal 0."));
            }

            var line = ReadLine();

            var resultColumns = string.IsNullOrEmpty(line)
                ? null
                : line.Split(new[] { _delimiter }, columnsIndexes.Max() + 2, StringSplitOptions.None);

            columns = IsNotNull(resultColumns)
                ? columnsIndexes.Select(resultColumns.ElementAtOrDefault)
                : null;

            return IsNotNull(resultColumns);
        }

        /// <summary>
        /// Mehod is deprecated please use bool Read(IEnumerable&lt;int&gt; columnsIndexes, out IEnumerable&lt;string&gt; columns) instead
        /// </summary>
        /// <param name="column1">Column1 value</param>
        /// <param name="column2">Column2 value</param>
        /// <returns>Continue read file</returns>
        [Obsolete("Mehod is deprecated please use bool Read(IEnumerable<int> columnsIndexes, out IEnumerable<string> columns) instead")]
        public bool Read(string column1, string column2)
        {
            IEnumerable<string> columns;
            
            return Read(new int[]{}, out columns);
        }

        /// <summary>
        /// Mehod is deprecated please use bool Read(IEnumerable&lt;int&gt; columnsIndexes, out IEnumerable&lt;string&gt; columns) instead
        /// </summary>
        /// <param name="column1">Column1 value</param>
        /// <param name="column2">Column2 value</param>
        /// <returns>Continue read file</returns>
        [Obsolete("Mehod is deprecated please use bool Read(IEnumerable<int> columnsIndexes, out IEnumerable<string> columns) instead")]
        public bool Read(out string column1, out string column2)
        {
            IEnumerable<string> columns;

            var continueRead = Read(new[] { 0, 1 }, out columns);

            column1 = GetColumnOrDefaul(columns, 0);
            column2 = GetColumnOrDefaul(columns, 1);

            return continueRead;
        }

        /// <summary>
        /// Close CSV reader writer
        /// </summary>
        public void Close()
        {
            CloseWriter();
            CloseReader();
        }

        #region Reader

        private void OpenReader(string fileName)
        {
            if (!IsNotNull(_readerStream))
            {
                try
                {
                    _readerStream = File.OpenText(fileName);
                }
                catch (UnauthorizedAccessException unauthorizedAccess)
                {
                    CloseAndThrowExcepton(new UnauthorizedAccessException(string.Format("CSV reader writer can't access file {0}", fileName), unauthorizedAccess));
                }
                catch(Exception exception)
                {
                    CloseAndThrowExcepton(new Exception("CSV reader writer failed to open reader", exception));
                }
            }
        }

        private string ReadLine()
        {
            if (!IsNotNull(_readerStream))
            {
                CloseAndThrowExcepton(new InvalidOperationException("CSV reader writer was not initialized for read operation."));
            }

            return _readerStream.ReadLine();
        }

        private void CloseReader()
        {
            if (IsNotNull(_readerStream))
            {
                _readerStream.Close();

                _readerStream = null;
            }
        }

        #endregion

        #region Writer

        private void OpenWriter(string fileName)
        {
            if (!IsNotNull(_writerStream))
            {
                try
                {
                    var fileInfo = new FileInfo(fileName);
                    _writerStream = fileInfo.CreateText();
                }
                catch (UnauthorizedAccessException unauthorizedAccess)
                {
                    CloseAndThrowExcepton(new UnauthorizedAccessException(string.Format("CSV reader writer can't access file {0}", fileName), unauthorizedAccess));
                }
                catch (Exception exception)
                {
                    CloseAndThrowExcepton(new Exception("CSV reader writer failed to open writer", exception));
                }
            }
        }

        private void WriteLine(string line)
        {
            if (!IsNotNull(_writerStream))
            {
                CloseAndThrowExcepton(new InvalidOperationException("CSV reader writer was not initialized for write operation."));
            }

            _writerStream.WriteLine(line);
        }

        private void CloseWriter()
        {
            if (IsNotNull(_writerStream))
            {
                _writerStream.Close();

                _writerStream = null;
            }
        }

        #endregion

        #region Helpers

        private void CloseAndThrowExcepton(Exception exception)
        {
            Close();

            throw exception;
        }

        private static bool IsNotNull<T>(T value)
            where T : class
        {
            return value != null;
        }

        private void CheckIsFileExistsOrCloseAndThrowException(string fileName)
        {
            if (!File.Exists(fileName))
            {
                CloseAndThrowExcepton(new FileNotFoundException("CSV reader writer can not open file specified.", fileName));
            }
        }

        private static string GetColumnOrDefaul(IEnumerable<string> columns, int columnIndex)
        {
            return IsNotNull(columns)
                ? columns.ElementAtOrDefault(columnIndex)
                : null;
        }

        #endregion
    }
}