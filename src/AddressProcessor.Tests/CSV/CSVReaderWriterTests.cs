﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

using AddressProcessing.CSV;

namespace Csv.Tests
{
    [TestFixture]
    public class CSVReaderWriterTests
    {
        private const string TestInputFile = @"test_data\contacts.csv";

        [TestCase(CSVReaderWriter.Mode.Read)]
        [TestCase(CSVReaderWriter.Mode.Write)]
        public void When_filePath_incorrect_Should_throw_fileNotFoundException(CSVReaderWriter.Mode mode)
        {
            var readerWriter = new CSVReaderWriter();

            TestDelegate readerWriterOpen = () => readerWriter.Open("fakefile.csv", mode);

            Assert.Throws<FileNotFoundException>(readerWriterOpen);
        }

        [Test]
        public void When_readWrite_mode_used_Should_throw_notSupportedException()
        {
            var readerWriter = new CSVReaderWriter();
            
            TestDelegate readerWriterOpen = () => readerWriter.Open(TestInputFile, CSVReaderWriter.Mode.Read | CSVReaderWriter.Mode.Write);

            Assert.Throws<NotSupportedException>(readerWriterOpen);
        }

        [Test]
        public void When_read_with_negative_indexes_used_Should_throw_argumentOutOfRangeException()
        {
            var readerWriter = new CSVReaderWriter();
            readerWriter.Open(TestInputFile, CSVReaderWriter.Mode.Read);

            TestDelegate readerWriterOpen = () =>
            {
                IEnumerable<string> columns;
                readerWriter.Read(new []{ 2, 3, -1 }, out columns);
            };

            Assert.Throws<ArgumentOutOfRangeException>(readerWriterOpen);
        }

        [TestCase("Shelby Macias", "3027 Lorem St.|Kokomo|Hertfordshire|L9T 3D5|Finland")]
        public void When_read_first_line_Should_return_correct_records(string column1, string column2)
        {
            var readerWriter = new CSVReaderWriter();
            readerWriter.Open(TestInputFile, CSVReaderWriter.Mode.Read);

            IEnumerable<string> columns;
            readerWriter.Read(new[] { 0, 1 }, out columns);

            readerWriter.Close();

            Assert.AreEqual(column1, columns.ElementAt(0));
            Assert.AreEqual(column2, columns.ElementAt(1));
        }
    }
}